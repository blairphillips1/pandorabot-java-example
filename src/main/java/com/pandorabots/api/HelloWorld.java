package com.pandorabots.api;

import org.json.JSONException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

/**
 * Created by blair on 18/04/19.
 */
public class HelloWorld {


    public static void main(String[] args) throws JSONException, IOException, URISyntaxException {
        outPut();
    }

    public static  PandorabotsAPI setUpBot() throws IOException, URISyntaxException {
        MagicParameters magicParameters = new MagicParameters();
        String cwd = System.getProperty("user.dir");
        magicParameters.readParameters(cwd + "/config.txt");
        PandorabotsAPI pandorabotsAPI = new PandorabotsAPI(magicParameters.getHostName(), magicParameters.getAppId()
                , magicParameters.getUserKey(), magicParameters.isDebug());

        pandorabotsAPI.deleteBot("blairbot");
        pandorabotsAPI.createBot("blairbot");
        String response;
        response = pandorabotsAPI.uploadFile("blairbot", "bot/blairbot/blair-bot.aiml");
        response = pandorabotsAPI.compileBot("blairbot");

        return pandorabotsAPI;
    }
    public static void outPut() throws IOException, URISyntaxException, JSONException {
        PandorabotsAPI pandorabotsAPI = setUpBot();
        String response;

        //taking in user input
        Scanner myObj = new Scanner(System.in);
        System.out.println("What do you need help with?");
        String userInput = myObj.nextLine();
        response = pandorabotsAPI.atalk("blairbot", userInput);

        System.out.println("Human: " + userInput);
        System.out.println("Robot: " + response);
    }
}
